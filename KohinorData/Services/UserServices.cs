﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using KohinorData.Models;
using KohinorData.DbAccess;
using Dapper;
using Microsoft.Data.SqlClient;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using static KohinorData.Models.User;

namespace KohinorData.Services
{
    public class UserServices: IUserServices
    {
        private readonly IConfiguration _configuration;
        public UserServices(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("Kdbs_KohinorWEB_Res");
            providerName = "System.Data.SqlClient";
        }

        public string ConnectionString { get; }
        public string providerName { get; }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConnectionString);
            }
        }
        public List<User> GetUserRegister()
        {
            List<User> result = new List<User>();
            try
            {
                using (IDbConnection dbconnection = Connection)
                {
                    dbconnection.Open();
                    result = dbconnection.Query<User>("[dbo].[SEG_S_CREDENCIALES]", commandType: CommandType.StoredProcedure).ToList();
                    dbconnection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return result;
            }

        }

		public string RegisterNewUser(User model)
		{

			using (IDbConnection dbconnection = Connection)
			{
				dbconnection.Open();
				var user = dbconnection.Query<List<User>>("[dbo].[SEG_M_USUARIO]", new { Action = "Add", RUCEMP = model.rucemp, CODUS1=model.codus1, NOMUSU=model.nomusu, USMAIL = model.usmail, FECEXP = "", CLAUSU="", CODSUC="", SERSEC="", PCNOM="", PCIP=model.pcip, PCMAC=model.pcmac, ESTADO="", TIPACC="", USUING=model.usuing, CODUSU = model.codusu }, commandType: CommandType.StoredProcedure).ToList();
				dbconnection.Close();
				
			}
			return "User registered Successfully";
		}
    }
    public interface IUserServices
    {
        public List<User> GetUserRegister();
        public string RegisterNewUser(User model);


	}
}
